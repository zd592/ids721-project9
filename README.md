## Deploy to Streamlit

https://ids721-project9-en9px55qbfxg6tbdvqfjaa.streamlit.app/

- ScreenShot:
    - ![Alt text](./images/5.jpg "Optional title1")

## Prerequisites

- Python 3.7 or higher
- pip (Python package installer)

## Installation

Follow these steps to set up the project environment and run the application:

### Step 1: Create the Repository

create a project

### Step 2: Create a Virtual Environment

Create a Python virtual environment to manage the project's dependencies independently from your global Python setup:

```bash
python3 -m venv .venv
source .venv/bin/activate  # On Unix or MacOS
.\.venv\Scripts\activate  # On Windows
```

### Step 3: Install Dependencies

Install the required Python packages using `pip`:

```bash
pip install streamlit transformers
```

### Step 4: Run the Application

Now you're ready to launch the app. Run the following command:

```bash
streamlit run app.py
```

This command will start the Streamlit server, and you should see output indicating the local URL where the app is running (usually `http://localhost:8501`).

## Usage

- Navigate to the provided URL in your web browser.
- Enter a starting text in the input field.
- Click the 'Generate Text' button to see the AI-generated text based on your input.

## Additional Information

For more information on Streamlit or the Transformers library, you can visit:
- Streamlit Documentation: https://docs.streamlit.io
- Hugging Face Transformers: https://huggingface.co/docs/transformers/index.html

## Screen shots:

-  results:
    - ![Alt text](./images/1.jpg "Optional title1")
    - ![Alt text](./images/2.jpg "Optional title1")
    - ![Alt text](./images/3.jpg "Optional title1")

- website:
    - ![Alt text](./images/4.jpg "Optional title1")

