import streamlit as st
from transformers import pipeline

# Set the title for the Streamlit web app
st.title('AI Text Generator using Hugging Face')

# User text entry
prompt_text = st.text_input("Input your text here:", "Start typing...")

# Text generation trigger button
if st.button('Create Text'):
    # Set up the model for generating text using GPT-2
    ai_text_gen = pipeline('text-generation', model='gpt2')
    
    # Perform text generation
    result_text = ai_text_gen(prompt_text, max_length=50, num_return_sequences=1)
    
    # Show the generated text in the app
    st.write(result_text[0]['generated_text'])

